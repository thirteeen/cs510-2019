package test;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class ExceptionTest {
    public static double divide(double a, double b){
        if (b == 0.0) {
            throw new ArithmeticException("Divided by zero!");
        }
        return a / b;
    }

    @Test (expected=ArithmeticException.class)
    public void TestA() {
        double result = divide(1, 0);
        System.out.println("testA");
    }

    @Test
    public void TestB() {
        try {
            double result = divide(1, 0);
            fail("My Exception was not thrown");
        } catch (Exception e) {
            assertNotNull(e.getMessage());
        }
    }
}
