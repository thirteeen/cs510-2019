package test;

import org.junit.*;

import java.io.*;
import static org.junit.Assert.*;

public class AssertOutputStreamTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    public void testA() {
        System.out.println("Hello World!");
        assertEquals(outContent.toString(), "Hello World!\n");
    }
}
