package test;

import org.junit.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class BeforeAndAfterNotationTest {
    private List<String> list;

    @Before
    public void init() {
        System.out.println("Init");
        list = new ArrayList<>(Arrays.asList("test1", "test2"));
    }

    @Test
    public void testA() {
        System.out.println("TestA");
        assertTrue(true);
    }

    @Test
    public void testB() {
        System.out.println("TestB");
        assertTrue(true);
    }


    @After
    public void finalize() {
        System.out.println("finalize");
        list.clear();
    }

}
