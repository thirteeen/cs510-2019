package test;

import org.junit.*;

import static org.junit.Assert.*;

public class BeforeClassAndAfterClassAnnotationsTest {
    @BeforeClass
    public static void setup() {
        System.out.println("startup - creating DB connection");
    }

    @Test
    public void testA() {
        System.out.println("TestA");
        assertTrue(true);
    }

    @Test
    public void testB() {
        System.out.println("TestB");
        assertTrue(true);
    }


    @AfterClass
    public static void tearDown() {
        System.out.println("closing DB connection");
    }

}
